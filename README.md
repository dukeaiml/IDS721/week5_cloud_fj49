# Rust Lambda Function connected with DynamoDB

## Overview

This project implements an AWS Lambda function in Rust using Cargo Lambda. The Lambda function receives a JSON payload containing three fields for id, name and age and then populates our DynamoDB database. The processed data can be triggered via API Gateway.

## Requirements

- Rust compiler and Cargo package manager installed.
- AWS IAM role with permission to create Lambda functions and invoke DynamoDB databases.
- API Gateway endpoint for triggering the Lambda function.

## Project Structure

1. **Lambda Function Code:**
   - Create a Rust project using Cargo.
   - Add dependencies for AWS Lambda and serde for JSON serialization.
   - Implement the Lambda function logic to speak to DynamoDB database and receive input via an API Gateway and write to the database
   
2. **API Gateway Integration:**
   - Configure an API Gateway endpoint to trigger the Lambda function.
   - Set up the necessary permissions and roles for the API Gateway to invoke the Lambda function.
   
3. **Data Processing:**
   - Define the sample data format that the Lambda function will receive (e.g., {"id": 1, "name": "Faraz Jawed", "age": 25}).
   - Process the incoming data and write in the database
   - Ensured error handling for invalid input or other runtime errors.
   

## Setup Instructions

1. Clone the repository:

   ```
   git clone https://gitlab.com/dukeaiml/IDS721/week5_cloud_fj49
   ```

2. Navigate to the project directory:

   ```
   cd week5_cloud_fj49
   ```

3. Build the project:

   ```
   cargo build --release
   ```

## Usage

- **Triggering the Lambda Function:**
  - Configure an API Gateway endpoint to trigger the Lambda function.
  - Send a JSON payload to the API Gateway endpoint.


## Screenshots 

### Deployed lambda function on AWS

![Screenshot 1](screenshots/lambda_func.png)

### Testing the function
![Screenshot 1](screenshots/testing_lambda.png)

### Testing Successful

![Screenshot 1](screenshots/testing_successful.png)

### DynamoDB populated

![Screenshot 1](screenshots/DynamoDB_populated.png)
-----

## License

This project is licensed under the [MIT License](LICENSE).


